import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlApplicationEngine

import resources

app = QGuiApplication(sys.argv)

engine = QQmlApplicationEngine()
engine.load(QUrl('ui/main.qml'))
engine.quit.connect(app.quit)

sys.exit(app.exec_())
