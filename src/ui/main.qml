import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.1

ApplicationWindow {
    id: window
    visible: true
    width: 800; height: 600

    title: "QML Controls 2.0 Demo v0.1"

    Dialog {
        id: dialog
        title: "Title"
        standardButtons: Dialog.Ok | Dialog.Cancel
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        modal: true

        Label {
            text: "I know this is awesome, but really awesome!"
        }

        onAccepted: console.log("Ok clicked")
        onRejected: console.log("Cancel clicked")
    }

    TabBar {
        id: bar
        width: parent.width
        TabButton {
            text: qsTr("Home")
        }
        TabButton {
            text: qsTr("Discover")
        }
        TabButton {
            text: qsTr("Activity")
        }
    }

    Column {
        anchors { left: parent.left; right: parent.right; top: parent.top; margins: 10; topMargin: bar.height + 10 }
        Button {
            text: "Show Dialog"
            onClicked: dialog.visible = true
        }
        CheckBox {
            text: qsTr("Breakfast")
            checked: true
        }
        TextField {
            placeholderText: "I am awesome. Right?"
        }
        SpinBox {
            value: 50
        }
        Slider {
            id: slider
            from: 1
            value: 25
            to: 100
            ToolTip {
                parent: slider.handle
                visible: slider.pressed
                text: slider.value.toFixed(1)
            }
        }
        GroupBox {
            title: qsTr("Synchronize")
            ColumnLayout {
                anchors.fill: parent
                CheckBox { text: qsTr("E-mail") }
                CheckBox { text: qsTr("Calendar") }
            }
        }

        ColumnLayout {
            Switch {
                text: qsTr("Wi-Fi")
            }
        }
    }
}
